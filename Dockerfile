FROM mcr.microsoft.com/playwright:v1.40.0-focal

# Limpar a pasta de cache do npm
RUN rm -rf /root/.npm

RUN apt-get update && apt-get -y install libnss3 libatk-bridge2.0-0 libdrm-dev libxkbcommon-dev libgbm-dev libasound-dev libatspi2.0-0 libxshmfence-dev zip unzip

# Install JAVA
RUN apt install -y default-jre
RUN wget -q https://github.com/allure-framework/allure2/releases/download/2.24.1/allure-2.24.1.tgz
RUN tar -zxvf allure-2.24.1.tgz -C /opt/
RUN ln -s /opt/allure-2.24.1/bin/allure /usr/bin/allure

# Removing folder allure .tgz after installed
RUN rm -R allure-2.24.1.tgz

#RUN chmod 777 -R /base-nodejs-bkd-qa

#RUN npm install

# RUN npm ci
# CMD npm run test

#RUN chmod 777 -R /base-nodejs-bkd-qa